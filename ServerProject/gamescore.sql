-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 
-- サーバのバージョン： 10.4.6-MariaDB
-- PHP のバージョン: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `techtest`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `gamescore`
--

CREATE TABLE `gamescore` (
  `Id` int(11) NOT NULL,
  `Score` int(11) NOT NULL,
  `Day` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `gamescore`
--

INSERT INTO `gamescore` (`Id`, `Score`, `Day`) VALUES
(1, 1000, '2019-10-01'),
(2, 2000, '2019-10-01'),
(3, 3000, '2019-10-01'),
(4, 4000, '2019-10-01'),
(5, 5000, '2019-10-01'),
(6, 0, '2019-10-11'),
(7, 29440, '2019-10-11'),
(8, 20150, '2019-10-13'),
(9, 30050, '2019-10-14'),
(10, 20180, '2019-10-15'),
(11, 24930, '2019-10-16');

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `gamescore`
--
ALTER TABLE `gamescore`
  ADD PRIMARY KEY (`Id`);

--
-- ダンプしたテーブルのAUTO_INCREMENT
--

--
-- テーブルのAUTO_INCREMENT `gamescore`
--
ALTER TABLE `gamescore`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
