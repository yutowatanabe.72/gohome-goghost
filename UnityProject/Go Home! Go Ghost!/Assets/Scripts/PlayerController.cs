﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    const float cameraPosY = 10f;
    const float cameraPosZ = -10f;

    public float speed = 100.0f;//キャラクターの移動スピード
    public float jumpPower = 1000f;//ジャンプの強さ
    bool canJump = true;//ジャンプ可能かどうかの判定
    bool dblJump = false;//二段ジャンプの一回目があったかどうかの判定
    bool feverEvent = false;//フィーバータイムの判定
    int feverCount = 0;//フィーバータイムになるために必要なアイテムのカウント
    const int needFeverCount = 20;//フィーバータイムに必要なアイテムカウント
    const float feverTime = 10f;//フィーバータイムの時間
    const float stageDist = 2000f;//プレイステージの距離
    const int recoverLevel1 = 1000;
    const int recoverLevel2 = 1500;
    const int recoverLevel3 = 2000;
    const int damageLevel1 = 1000;
    const int damageLevel2 = 2000;
    const int damageLevel3 = 3000;

    float delta = 0;//フィーバータイムの制限時間に使用
    float destroyTime = 0.25f;//アイテムの消滅時間

    int scoreA;//10点アイテムのカウント数
    int scoreB;//50点アイテムのカウント数
    int scoreC;//100点アイテムのカウント数
    const int scoreMagB = 5;//スコアの倍率
    const int scoreMagC = 10;
    bool getItem = false;//得点アイテム取得の判定

    

    public GameObject toresultButton;//リザルトシーンへのシーン遷移ボタン
    public GameObject Clear;//クリア表示
    public GameObject GameOver;//ゲームオーバー表示
    public GameObject winIrast;//ゲームクリア時のキャラクターのイラスト
    public GameObject loseIrast;//ゲームオーバー時のキャラクターのイラスト
    public GameObject defIrast;//初期イラストの表示
    public GameObject feverIrast;//フィーバータイム中のイラスト
    public Text distansText;//ゴールまでの距離の表示
    public GameObject chasecamera;

    Rigidbody2D rb2d;
    [SerializeField] ContactFilter2D filter2d;

    void Start()
    {
        //スコアのリセット
        scoreA = 0;
        scoreB = 0;
        scoreC = 0;
        rb2d = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        this.delta -= Time.deltaTime;

        //カメラの自動追尾
        chasecamera.transform.position = new Vector3(transform.position.x, transform.position.y + cameraPosY, cameraPosZ);

        //キャラクターの自動移動
        rb2d.velocity = new Vector2(speed, rb2d.velocity.y);

        //ゲームオーバーになった時の処理
        if (this.transform.position.x >= stageDist || LifeGage.playerLife <= 0)
        {
            speed = 0;//移動の終了
            canJump = false;//ジャンプ不可
            LifeGage.endflag = true;//体力ゲージの自動減少の停止
            toresultButton.SetActive(true);//シーン遷移のボタン表示

            if (LifeGage.playerLife <= 0)
            {
                //ゲームオーバー時のUI変更
                loseIrast.transform.position = new Vector3(transform.position.x, transform.position.y, 1);
                GameOver.SetActive(true);
                defIrast.SetActive(false);
                loseIrast.SetActive(true);
            }
            else
            {
                //ゲームクリア時のUI変更
                winIrast.transform.position = new Vector3(transform.position.x, transform.position.y, 1);
                Clear.SetActive(true);
                defIrast.SetActive(false);
                winIrast.SetActive(true);
            }
        }

        if (getItem)
        {
            //フィーバータイムへのカウントの加算
            feverCount++;
            getItem = false;
        }

        //フィーバータイムの判定
        if (!feverEvent)
        {
            if (delta <= 0)
            {
                if (feverCount >= needFeverCount)
                {
                    delta = feverTime;
                    feverEvent = true;
                    feverIrast.transform.position = new Vector3(transform.position.x, transform.position.y, 1);
                    feverIrast.SetActive(true);
                }
            }
        }
        else
        {
            feverIrast.transform.position = new Vector3(transform.position.x, transform.position.y, 1);
            feverCount = 0;
        }

        //フィーバータイムの時間制限判定
        if(delta <= 0)
        {
            dblJump = false;
            feverEvent = false;
            feverIrast.SetActive(false);
        }
        
        //キャラクターのジャンプ処理
        if (Input.GetKeyDown("space"))
        {
            if (canJump)
            {
                rb2d.AddForce(Vector2.up * jumpPower);
                if (!feverEvent)
                {
                    canJump = false;
                }
                else
                {
                    //フィーバータイム中の二段ジャンプ処理
                    if (!dblJump)
                    {
                        dblJump = true;
                    }
                    else
                    {
                        canJump = false;
                    }
                }
            }
        }
        //得点処理→Mainに渡すために数字を簡略化
        LifeGage.totalCount = scoreA + (scoreB * scoreMagB) + (scoreC * scoreMagC);

        //ゴールまでの距離の表示
        distansText.text = "ゴールまでの距離\n" + Mathf.Floor(transform.position.x).ToString() + "/" + stageDist.ToString();
    }

    public void OnCollisionEnter2D(Collision2D hit)
    {
        //ジャンプカウントのリセット
        if (hit.gameObject.CompareTag("Ground") || hit.gameObject.CompareTag("Block"))
        {
            dblJump = false;
            canJump = true;
        }
    }

    void OnTriggerEnter2D(Collider2D hit)
    {
        //想定外のジャンプが起こらないようにするための処理
        if (hit.gameObject.tag == "Block")
        {
            canJump = false;
        }

        if (hit.gameObject.tag == "jump")
        {
            canJump = true;
            dblJump = false;
        }


        //アイテムと接触したときの処理
        //ライフゲージの回復・得点の加算
        if (hit.gameObject.tag == "recover1")
        {
            hit.transform.localScale = new Vector2(1, 1);
            Destroy(hit.gameObject, destroyTime);
            LifeGage.playerLife += recoverLevel1;
            scoreA++;
            getItem = true;
        }

        if (hit.gameObject.tag == "recover2")
        {
            hit.transform.localScale = new Vector2(1, 1);
            Destroy(hit.gameObject, destroyTime);
            LifeGage.playerLife += recoverLevel2;
            scoreB++;
            getItem = true;
        }

        if (hit.gameObject.tag == "recover3")
        {
            hit.transform.localScale = new Vector2(1, 1);
            Destroy(hit.gameObject,destroyTime);
            LifeGage.playerLife += recoverLevel3;
            scoreC++;
            getItem = true;
        }

        //ライフゲージにダメージ
        if (hit.gameObject.tag == "damage1")
        {
            Destroy(hit.gameObject);
            LifeGage.playerLife -= damageLevel1;
            transform.position = new Vector2(transform.position.x - 2, transform.position.y);
        }

        if (hit.gameObject.tag == "damage2")
        {
            Destroy(hit.gameObject);
            LifeGage.playerLife -= damageLevel2;
            transform.position = new Vector2(transform.position.x - 2, transform.position.y);
        }

        if (hit.gameObject.tag == "damage3")
        {
            Destroy(hit.gameObject);
            LifeGage.playerLife -= damageLevel3;
            transform.position = new Vector2(transform.position.x - 2, transform.position.y);
        }
    }
}
