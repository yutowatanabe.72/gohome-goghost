﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomCreate : MonoBehaviour
{
    //ライフゲージ回復・得点加算アイテム
    public GameObject Item1;
    public GameObject Item2;
    public GameObject Item3;
    //ダメージアイテム
    public GameObject DamageItem1;
    public GameObject DamageItem2;
    public GameObject DamageItem3;
    //床の生成
    public GameObject floar;
    public GameObject floar2;//空中ジャンプをしないようにするためのオブジェクト

    public GameObject player;

    float delta = 0;//床のランダム生成に使用
    float delta2 = 0;//アイテムのランダム生成に使用
    const float floarSpan = 0.5f;//床の生成頻度
    const float itemCreateSpan = 0.2f;//アイテムの生成頻度
    const int createPosX = 50;


    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        this.delta2 += Time.deltaTime;

        //床の生成
        if (player.transform.position.x < 1940)
        {

            if (this.delta >= floarSpan)
            {
                this.delta = 0;
                GameObject Floar = Instantiate(floar) as GameObject;
                GameObject Floar2 = Instantiate(floar2) as GameObject;
                int rnd = Random.Range(0, 5);

                switch (rnd)
                {
                    case 0:
                    case 2:
                        Floar.transform.position = new Vector3(player.transform.position.x + createPosX, -4, 0);
                        Floar2.transform.position = new Vector3(player.transform.position.x + createPosX, -4, 0);
                        break;

                    case 1:
                    case 3:
                        Floar.transform.position = new Vector3(player.transform.position.x + createPosX, 10, 0);
                        Floar2.transform.position = new Vector3(player.transform.position.x + createPosX, 10, 0);
                        break;

                    default:
                        Floar.transform.position = new Vector3(player.transform.position.x + createPosX, 24, 0);
                        Floar2.transform.position = new Vector3(player.transform.position.x + createPosX, 24, 0);
                        break;
                }
            }

            //アイテムの生成
            if (this.delta2 >= itemCreateSpan)
            {
                this.delta2 = 0;
                int ran = Random.Range(0, 13);
                GameObject Item;

                switch (ran)
                {
                    case 0:
                    case 5:
                    case 9:
                    case 10:
                        Item = Instantiate(Item1) as GameObject;
                        break;

                    case 1:
                    case 6:
                    case 11:
                        Item = Instantiate(Item2) as GameObject;
                        break;

                    case 2:
                    case 7:
                        Item = Instantiate(Item3) as GameObject;
                        break;

                    case 3:
                    case 8:
                        Item = Instantiate(DamageItem1) as GameObject;
                        break;

                    case 4:
                        Item = Instantiate(DamageItem2) as GameObject;
                        break;

                    default:
                        Item = Instantiate(DamageItem3) as GameObject;
                        break;
                }

                int rnd = Random.Range(0, 16);

                switch (rnd)
                {
                    case 0:
                    case 5:
                    case 9:
                    case 12:
                    case 14:
                        Item.transform.position = new Vector3(player.transform.position.x + createPosX, -12, 0);
                        break;

                    case 1:
                    case 6:
                    case 10:
                    case 13:
                        Item.transform.position = new Vector3(player.transform.position.x + createPosX, 2, 0);
                        break;

                    case 2:
                    case 7:
                    case 11:
                        Item.transform.position = new Vector3(player.transform.position.x + createPosX, 16, 0);
                        break;

                    case 3:
                    case 8:
                        Item.transform.position = new Vector3(player.transform.position.x + createPosX, 30, 0);
                        break;

                    default:
                        Item.transform.position = new Vector3(player.transform.position.x + createPosX, 44, 0);
                        break;
                }
            }
        }
    }
}
